import { ComponentFixture, TestBed, async, inject, tick, fakeAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { HomeComponent } from './home.component';
import { By } from '@angular/platform-browser';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let de: DebugElement;

  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checking mocked data for subreddit', () => {
    expect(component.sub.length).toEqual(8);
  });

  it('checking mocked data for post', () => {
    expect(component.post.length).toEqual(3);
  });

  it('checking click button for creating sub',fakeAsync(()=>{
    fixture.detectChanges();
    spyOn(component,'createSub');

    let btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click',null);
    tick();
    fixture.detectChanges();
    expect(component.createSub).toHaveBeenCalled();
  }))

  it('checking click button for creating post',fakeAsync(()=>{
    fixture.detectChanges();
    spyOn(component,'createPost');

    let btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click',null);
    tick();
    fixture.detectChanges();
    expect(component.createPost).not.toHaveBeenCalled();

  }))

  
  it('checking upvote button',fakeAsync(()=>{
    fixture.detectChanges();
    spyOn(component,'upvote');

    let btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click',null);
    tick();
    fixture.detectChanges();
    expect(component.upvote).not.toHaveBeenCalled();
  }))

  
  it('checking click button for creating post',fakeAsync(()=>{
    fixture.detectChanges();
    spyOn(component,'downvote');

    let btn = fixture.debugElement.query(By.css('button'));
    btn.triggerEventHandler('click',null);
    tick();
    fixture.detectChanges();
    expect(component.downvote).not.toHaveBeenCalled();

  }))

});
