import { Component, OnInit } from '@angular/core';
import { SubReddit, Post  } from '../models/post';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})


export class HomeComponent implements OnInit {


  constructor() { }
  
  sub : SubReddit[] = []
  post: Post[] = []

  ngOnInit(): void {
    let mockedSub = new SubReddit("Texas")
    let mockedSub1 = new SubReddit("NewYork")
    let mockedSub2 = new SubReddit("Washington")
    let mockedSub3 = new SubReddit("Michigan")
    let mockedSub4 = new SubReddit("Floria")
    let mockedSub5 = new SubReddit("Alabama")
    let mockedSub6 = new SubReddit("NewMexico")
    let mockedSub7 = new SubReddit("Massachusetts")


    let mockedPost = new Post("Texas","Best place to live in Texas","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et malesuada fames ac turpis egestas sed tempus urna. Laoreet id donec ultrices tincidunt.",5)
    let mockedPost1 = new Post("Cali","Best place to live in Cali","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et malesuada fames ac turpis egestas sed tempus urna. Laoreet id donec ultrices tincidunt.",5)
    let mockedPost2 = new Post("NewYork","Best place to live in NewYork","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et malesuada fames ac turpis egestas sed tempus urna. Laoreet id donec ultrices tincidunt.",5)


    this.sub.push(mockedSub)
    this.sub.push(mockedSub1)
    this.sub.push(mockedSub2)
    this.sub.push(mockedSub3)
    this.sub.push(mockedSub4)
    this.sub.push(mockedSub5)
    this.sub.push(mockedSub6)
    this.sub.push(mockedSub7)


    this.post.push(mockedPost)
    this.post.push(mockedPost1)
    this.post.push(mockedPost2)
  }

  subName = '';
  getName(){
    return this.subName
  }
  setName(text:any){
    this.subName = text
  }

  createSub(name:any){
    if(name.length === 0){
      
    }else{
      let newSub = new SubReddit(name.value);
      this.sub.push(newSub)
      this.subName = ""
    }
  }

  createPost(name:any, title:any, body:any){
    if(name.length === 0 || title.length === 0 || body.length === 0){
      // prompt input
    }else{
      let newSub = new SubReddit(name.value)
      let newPost = new Post(name.value, title.value,body.value,1)
      this.post.push(newPost)
      this.sub.push(newSub)
    }
    
    name.value = "";
    title.value = "";
    body.value= "";
  }

  upvote(item:any){
    item.likes +=1
  }

  downvote(item:any){
    item.likes -=1
  }
}
