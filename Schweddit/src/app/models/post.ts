export class SubReddit{
    name!: string;
    constructor(name:string){
        this.name = "r/"+ name
    }
}

export class Post extends SubReddit{
    title!:string;
    body!:string;
    likes!:number;

    constructor(name: string, title:string, body:string, likes:number){
        super(name);
        this.title = title
        this.body = body
        this.likes = likes
    }

    changeTitle(title:string){
        this.title = title;
    }

    changeBody(body:string){
        this.body = body
    }


}