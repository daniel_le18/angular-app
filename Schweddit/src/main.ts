import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { defineCustomElements } from '@schwab/web-components/dist/custom-elements-es5';
import { SdpsButton, SdpsInput } from '@schwab/web-components/dist/custom-elements-es5';
 
customElements.define('sdps-button', SdpsButton);
customElements.define('sdps-input', SdpsInput);
defineCustomElements(window);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
